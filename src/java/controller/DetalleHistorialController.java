package controller;

import entities.DetalleHistorial;
import facade.DetalleHistorialFacade;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.annotation.PostConstruct;

@ManagedBean(name = "detalleHistorialController")
@ViewScoped
public class DetalleHistorialController extends AbstractController<DetalleHistorial> implements Serializable {

    @EJB
    private DetalleHistorialFacade ejbFacade;

    @PostConstruct
    @Override
    public void init() {
        super.setFacade(ejbFacade);
    }

    public DetalleHistorialController() {
        super(DetalleHistorial.class);
    }

}
