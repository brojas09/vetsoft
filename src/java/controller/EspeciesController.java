package controller;

import entities.Especies;
import facade.EspeciesFacade;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.annotation.PostConstruct;

@ManagedBean(name = "especiesController")
@ViewScoped
public class EspeciesController extends AbstractController<Especies> implements Serializable {

    @EJB
    private EspeciesFacade ejbFacade;

    @PostConstruct
    @Override
    public void init() {
        super.setFacade(ejbFacade);
    }

    public EspeciesController() {
        super(Especies.class);
    }

}
