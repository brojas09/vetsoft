package controller;

import controller.login.LoginBean;
import controller.util.SessionUtils;
import entities.Especies;
import entities.Mascotas;
import entities.Veterinarias;
import facade.MascotasFacade;
import facade.VeterinariasFacade;
import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ApplicationScoped;
import javax.faces.event.ActionEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

@ManagedBean(name = "mascotasController")
@SessionScoped
@ApplicationScoped
public class MascotasController extends AbstractController<Mascotas> implements Serializable {

    private static final Logger LOGGER = Logger.getLogger(MascotasController.class.getName());

    @EJB
    private MascotasFacade ejbFacade;

    @EJB
    private VeterinariasFacade veterinariasFacade;

    @PostConstruct
    @Override
    public void init() {
        super.setFacade(ejbFacade);
    }

    public MascotasController() {
        super(Mascotas.class);
    }
    private final LoginBean loginBean = new LoginBean();

    @Override
    public void saveNew(ActionEvent event) {
        super.getSelected().setFechaAlta(Calendar.getInstance().getTime());
        super.getSelected().setArchivo(archivo.getContents());
        super.getSelected().setMime(archivo.getContentType());
        if (!loginBean.tieneRol("1")) {
            LoadVeterinaria();
        }
        super.saveNew(event);
        this.archivo = null;
    }

    @Override
    public void save(ActionEvent event) {
        super.getSelected().setArchivo(archivo.getContents());
        super.getSelected().setMime(archivo.getContentType());
        if (!loginBean.tieneRol("1")) {
            LoadVeterinaria();
        }

        super.save(event);
        this.archivo = null;

    }

    private void LoadVeterinaria() {
        javax.servlet.http.HttpSession session = SessionUtils.getSession();
        Integer idVeterinaria = Integer.parseInt(session.getAttribute("veterinaria").toString());
        Veterinarias v = veterinariasFacade.find(idVeterinaria);
        super.getSelected().setIdVeterinaria(v);
    }
    private Especies especie;

    public Especies getEspecie() {
        return especie;
    }

    public void setEspecie(Especies especie) {
        this.especie = especie;
    }

    @Override
    public Collection<Mascotas> getItems() {
        if (loginBean.tieneRol("1")) {
            return super.getItems();
        } else {
            javax.servlet.http.HttpSession session = SessionUtils.getSession();
            Integer idVeterinaria = Integer.parseInt(session.getAttribute("veterinaria").toString());
            return ejbFacade.findAll(idVeterinaria);
        }
    }

    private UploadedFile archivo;

    public UploadedFile getArchivo() {
        return archivo;
    }

    public void setArchivo(UploadedFile archivo) {
        this.archivo = archivo;
    }

    @Override
    public Mascotas prepareCreate(ActionEvent event) {
        this.archivo = null;
        return super.prepareCreate(event); //To change body of generated methods, choose Tools | Templates.
    }

    public void upload(FileUploadEvent event) {
        this.archivo = event.getFile();
        LOGGER.log(Level.INFO, "Uploaded File {0} size: {1}", new Object[]{event.getFile().getFileName(), Long.toString(event.getFile().getSize())});
    }

    private StreamedContent archivoStream;
    public StreamedContent getArchivoStream() {
        Mascotas masc = super.getSelected();
        try {
            ByteArrayInputStream stream = new ByteArrayInputStream(masc.getArchivo());
            archivoStream = new DefaultStreamedContent(stream, masc.getMime());
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "ERROR", e);
            archivoStream = new DefaultStreamedContent();
        }
        return archivoStream;
    }
}
