package controller;

import entities.Permisos;
import facade.PermisosFacade;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.annotation.PostConstruct;

@ManagedBean(name = "permisosController")
@ViewScoped
public class PermisosController extends AbstractController<Permisos> implements Serializable {

    @EJB
    private PermisosFacade ejbFacade;

    @PostConstruct
    @Override
    public void init() {
        super.setFacade(ejbFacade);
    }

    public PermisosController() {
        super(Permisos.class);
    }

}
