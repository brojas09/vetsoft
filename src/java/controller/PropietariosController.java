package controller;

import controller.login.LoginBean;
import controller.util.SessionUtils;
import entities.Propietarios;
import entities.Veterinarias;
import facade.PropietariosFacade;
import facade.VeterinariasFacade;
import java.io.Serializable;
import java.util.Collection;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;

@ManagedBean(name = "propietariosController")
@ViewScoped
public class PropietariosController extends AbstractController<Propietarios> implements Serializable {

    @EJB
    private VeterinariasFacade veterinariasFacade;

    @EJB
    private PropietariosFacade ejbFacade;

    @PostConstruct
    @Override
    public void init() {
        super.setFacade(ejbFacade);
    }

    public PropietariosController() {
        super(Propietarios.class);
    }
    private final LoginBean loginBean = new LoginBean();

    @Override
    public void saveNew(ActionEvent event) {
        if (!loginBean.tieneRol("1")) {
            LoadVeterinaria();
        }
        super.saveNew(event);
    }

    @Override
    public void save(ActionEvent event) {
        if (!loginBean.tieneRol("1")) {
            LoadVeterinaria();
        }
        super.save(event);
    }

    private void LoadVeterinaria() {
        javax.servlet.http.HttpSession session = SessionUtils.getSession();
        Integer idVeterinaria = Integer.parseInt(session.getAttribute("veterinaria").toString());
        Veterinarias v = veterinariasFacade.find(idVeterinaria);
        super.getSelected().setIdVeterinaria(v);
    }

    @Override
    public Collection<Propietarios> getItems() {
        if (loginBean.tieneRol("1")) {
            return super.getItems();
        } else {
            javax.servlet.http.HttpSession session = SessionUtils.getSession();
            Integer idVeterinaria = Integer.parseInt(session.getAttribute("veterinaria").toString());
            return ejbFacade.findAll(idVeterinaria);
        }
    }

}
