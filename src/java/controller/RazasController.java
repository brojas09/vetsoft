package controller;

import entities.Razas;
import facade.RazasFacade;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.annotation.PostConstruct;

@ManagedBean(name = "razasController")
@ViewScoped
public class RazasController extends AbstractController<Razas> implements Serializable {

    @EJB
    private RazasFacade ejbFacade;

    @PostConstruct
    @Override
    public void init() {
        super.setFacade(ejbFacade);
    }

    public RazasController() {
        super(Razas.class);
    }

}
