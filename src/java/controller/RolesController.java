package controller;

import entities.Roles;
import facade.RolesFacade;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.annotation.PostConstruct;

@ManagedBean(name = "rolesController")
@ViewScoped
public class RolesController extends AbstractController<Roles> implements Serializable {

    @EJB
    private RolesFacade ejbFacade;

    @PostConstruct
    @Override
    public void init() {
        super.setFacade(ejbFacade);
    }

    public RolesController() {
        super(Roles.class);
    }

}
