package controller;

import controller.login.LoginBean;
import controller.util.HashTextTest;
import entities.Usuarios;
import facade.UsuariosFacade;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

@ManagedBean(name = "usuariosController")
@ViewScoped
public class UsuariosController extends AbstractController<Usuarios> implements Serializable {

    @EJB
    private UsuariosFacade ejbFacade;

    @PostConstruct
    @Override
    public void init() {
        super.setFacade(ejbFacade);
    }

    public UsuariosController() {
        super(Usuarios.class);
    }

    @Override
    public void saveNew(ActionEvent event) {
        super.getSelected().setPassword(HashTextTest.sha1(super.getSelected().getPassword()));
        super.saveNew(event);
    }

    @Override
    public void save(ActionEvent event) {
        super.getSelected().setPassword(HashTextTest.sha1(super.getSelected().getPassword()));
        super.save(event);
    }

    private String newPass1;
    private String newPass2;
    private String oldPass;

    public String getNewPass1() {
        return newPass1;
    }

    public void setNewPass1(String newPass1) {
        this.newPass1 = newPass1;
    }

    public String getNewPass2() {
        return newPass2;
    }

    public void setNewPass2(String newPass2) {
        this.newPass2 = newPass2;
    }

    public String getOldPass() {
        return oldPass;
    }

    public void setOldPass(String oldPass) {
        this.oldPass = oldPass;
    }

    public void updatePass(ActionEvent event) {
        FacesMessage message;
        FacesContext context = FacesContext.getCurrentInstance();
        if (newPass1.matches(newPass2)) {
            //recuperar usuario
            LoginBean lb = new LoginBean();
            Usuarios u = ejbFacade.find(lb.getUsuario());
            //comparar contraseña actual
            if (u.getPassword().matches(HashTextTest.sha1(oldPass))) {
                u.setPassword(HashTextTest.sha1(newPass1));
                //actualizar bd
                int i = ejbFacade.updatePass(u);
                System.out.println(i);
                if (i == 1) {
                    message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Éxito", "Actualización realizada con éxito");
                    context.addMessage(null, message);

                } else {
                    message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "No se pudo actualizar en la Base de Datos");
                    context.addMessage(null, message);

                }

                //RequestContext.getCurrentInstance().showMessageInDialog(message);
            } else {
                message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "Las contraseña anterior no coincide. Intente de nuevo");
                context.addMessage(null, message);
                //RequestContext.getCurrentInstance().showMessageInDialog(message);
            }
        } else {
            message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "Las contraseñas no coinciden. Intente de nuevo");
            context.addMessage(null, message);
            //RequestContext.getCurrentInstance().showMessageInDialog(message);
        }
    }
    private Usuarios user;

    public Usuarios getUser() {
        LoginBean lb = new LoginBean();
        user = ejbFacade.find(lb.getUsuario());
        return user;
    }

    public void setUser(Usuarios user) {
        this.user = user;
    }

    public void saveUser(ActionEvent event) {
        FacesMessage message;
        FacesContext context = FacesContext.getCurrentInstance();
        LoginBean lb = new LoginBean();
        Usuarios u = ejbFacade.find(lb.getUsuario());
        //comparar contraseña actual

        u.setEmail(user.getEmail());
        u.setTelefono(user.getTelefono());
        int i = ejbFacade.updateUser(u);
        System.out.println(i);
        if (i == 1) {
            message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Éxito", "Actualización realizada con éxito");
            context.addMessage(null, message);

        } else {
            message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "No se pudo actualizar en la Base de Datos");
            context.addMessage(null, message);

        }

        //RequestContext.getCurrentInstance().showMessageInDialog(message);
    }
}
