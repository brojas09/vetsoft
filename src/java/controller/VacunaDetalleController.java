package controller;

import entities.VacunaDetalle;
import facade.VacunaDetalleFacade;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.annotation.PostConstruct;

@ManagedBean(name = "vacunaDetalleController")
@ViewScoped
public class VacunaDetalleController extends AbstractController<VacunaDetalle> implements Serializable {

    @EJB
    private VacunaDetalleFacade ejbFacade;

    @PostConstruct
    @Override
    public void init() {
        super.setFacade(ejbFacade);
    }

    public VacunaDetalleController() {
        super(VacunaDetalle.class);
    }

    @Override
    protected void setEmbeddableKeys() {
        this.getSelected().getVacunaDetallePK().setIdDetalle(this.getSelected().getDetalleHistorial().getIdDetalle());
        this.getSelected().getVacunaDetallePK().setIdVacuna(this.getSelected().getVacunas().getIdVacuna());
    }

    @Override
    protected void initializeEmbeddableKey() {
        this.getSelected().setVacunaDetallePK(new entities.VacunaDetallePK());
    }

}
