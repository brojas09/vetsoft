package controller;

import entities.Vacunas;
import facade.VacunasFacade;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.annotation.PostConstruct;

@ManagedBean(name = "vacunasController")
@ViewScoped
public class VacunasController extends AbstractController<Vacunas> implements Serializable {

    @EJB
    private VacunasFacade ejbFacade;

    @PostConstruct
    @Override
    public void init() {
        super.setFacade(ejbFacade);
    }

    public VacunasController() {
        super(Vacunas.class);
    }

}
