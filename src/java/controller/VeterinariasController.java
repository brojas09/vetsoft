package controller;

import entities.Veterinarias;
import facade.VeterinariasFacade;
import java.io.Serializable;
import java.util.Calendar;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;

@ManagedBean(name = "veterinariasController")
@ViewScoped
public class VeterinariasController extends AbstractController<Veterinarias> implements Serializable {

    @EJB
    private VeterinariasFacade ejbFacade;

    @PostConstruct
    @Override
    public void init() {
        super.setFacade(ejbFacade);
    }

    public VeterinariasController() {
        super(Veterinarias.class);
    }

    @Override
    public void saveNew(ActionEvent event) {
        this.getSelected().setFechaAlta(Calendar.getInstance().getTime());
        this.getSelected().setEstado(Boolean.TRUE);
        super.saveNew(event);
    }

    @Override
    public void save(ActionEvent event) {
        super.save(event);
    }
}
