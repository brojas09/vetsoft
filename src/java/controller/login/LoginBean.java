/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.login;

import controller.util.HashTextTest;
import controller.util.SessionUtils;
import entities.Usuarios;
import java.util.Calendar;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.context.RequestContext;
import facade.UsuariosFacade;

/**
 *
 * @author javie
 */
@ManagedBean
@SessionScoped
public class LoginBean {

    public Integer getUsuario() {
        javax.servlet.http.HttpSession session = SessionUtils.getSession();
        Integer idUser = Integer.parseInt(session.getAttribute("idUser").toString());
        return idUser;
    }

    @EJB
    private UsuariosFacade usuarioFacade;

    private Usuarios usuario = new Usuarios();
    private String user;
    private String pass;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    /**
     * Creates a new instance of LoginBean
     */
    public LoginBean() {
    }

    public String login(ActionEvent event) {
        RequestContext context = RequestContext.getCurrentInstance();
        FacesMessage message = null;
        try {
            usuario = usuarioFacade.findByUsuario(user);
        } catch (Exception e) {
            System.out.println(e);
        }
        if (usuario != null && usuario.getPassword().compareTo(HashTextTest.sha1(pass)) == 0 && usuario.getActivo()) {
            logeado = true;
            message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Bienvenido", usuario.getNombre());
            FacesContext.getCurrentInstance().addMessage(null, message);

        } else if (!usuario.getActivo()) {
            message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error de Login", "Usuario Inactivo");
            FacesContext.getCurrentInstance().addMessage(null, message);
            return null;
        } else {
            message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error de Login", "Credenciales Invalidas");
            FacesContext.getCurrentInstance().addMessage(null, message);
            return null;
        }

        context.addCallbackParam("estaLogeado", logeado);
        if (logeado) {
            javax.servlet.http.HttpSession session = SessionUtils.getSession();
            session.setAttribute("username", usuario.getUsuario());
            session.setAttribute("idUser", usuario.getIdUsuario());
            session.setAttribute("Rol", usuario.getIdRol().getIdRol());
            session.setAttribute("estaLogueado", logeado);
            session.setAttribute("veterinaria", usuario.getIdVeterinaria().getIdVeterinaria());

            //return "/secured/home.xhtml?faces-redirect=true";
            usuario.setUltimoIngreso(Calendar.getInstance().getTime());
            usuarioFacade.edit(usuario);
            return "/home.xhtml?faces-redirect=true";

        }
        return null;
    }

    //String tipo;
    public String getTipo() {
        javax.servlet.http.HttpSession session = SessionUtils.getSession();
        String tipo = session.getAttribute("tipoUser").toString();
        return tipo;
    }

    public void submit() {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Correct", "Correct");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    private boolean logeado = false;

    public boolean estaLogeado() {
        return logeado;
    }

    public String logout() {
        FacesContext.getCurrentInstance().getExternalContext()
                .invalidateSession();
        System.out.println("logout successful");
        logeado = false;
        return "/login.xhtml?faces-redirect=true";
    }

    public String isLoggedInForwardHome() {
        if (logeado) {
            return "/secure/index.xhtml?faces-redirect=true";
        }
        return null;
    }

    public Boolean tieneRol(String idRol) {
        Integer nivelRol = getRol();
        return (nivelRol <= (Integer.parseInt(idRol)));
    }

    public Integer getRol() {
        javax.servlet.http.HttpSession session = SessionUtils.getSession();
        rol = Integer.parseInt(session.getAttribute("Rol").toString());
        return rol;
    }
    private Integer rol;

}
