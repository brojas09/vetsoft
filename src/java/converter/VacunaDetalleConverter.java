package converter;

import facade.VacunaDetalleFacade;
import entities.VacunaDetalle;
import controller.util.JsfUtil;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter(value = "vacunaDetalleConverter")
public class VacunaDetalleConverter implements Converter {

    @EJB
    private VacunaDetalleFacade ejbFacade;

    private static final String SEPARATOR = "#";
    private static final String SEPARATOR_ESCAPED = "\\#";

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
        if (value == null || value.length() == 0 || JsfUtil.isDummySelectItem(component, value)) {
            return null;
        }
        return this.ejbFacade.find(getKey(value));
    }

    entities.VacunaDetallePK getKey(String value) {
        entities.VacunaDetallePK key;
        String values[] = value.split(SEPARATOR_ESCAPED);
        key = new entities.VacunaDetallePK();
        key.setIdDetalle(Integer.parseInt(values[0]));
        key.setIdVacuna(Integer.parseInt(values[1]));
        return key;
    }

    String getStringKey(entities.VacunaDetallePK value) {
        StringBuilder sb = new StringBuilder();
        sb.append(value.getIdDetalle());
        sb.append(SEPARATOR);
        sb.append(value.getIdVacuna());
        return sb.toString();
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
        if (object == null
                || (object instanceof String && ((String) object).length() == 0)) {
            return null;
        }
        if (object instanceof VacunaDetalle) {
            VacunaDetalle o = (VacunaDetalle) object;
            return getStringKey(o.getVacunaDetallePK());
        } else {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), VacunaDetalle.class.getName()});
            return null;
        }
    }

}
