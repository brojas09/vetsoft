/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author broja
 */
@Entity
@Table(name = "detalle_historial", catalog = "vetsoft", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetalleHistorial.findAll", query = "SELECT d FROM DetalleHistorial d"),
    @NamedQuery(name = "DetalleHistorial.findByIdDetalle", query = "SELECT d FROM DetalleHistorial d WHERE d.idDetalle = :idDetalle"),
    @NamedQuery(name = "DetalleHistorial.findByComentario", query = "SELECT d FROM DetalleHistorial d WHERE d.comentario = :comentario"),
    @NamedQuery(name = "DetalleHistorial.findByFechaConsulta", query = "SELECT d FROM DetalleHistorial d WHERE d.fechaConsulta = :fechaConsulta"),
    @NamedQuery(name = "DetalleHistorial.findByTipoImagen", query = "SELECT d FROM DetalleHistorial d WHERE d.tipoImagen = :tipoImagen")})
public class DetalleHistorial implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_detalle")
    private Integer idDetalle;
    @Size(max = 2147483647)
    @Column(name = "comentario")
    private String comentario;
    @Column(name = "fecha_consulta")
    @Temporal(TemporalType.TIME)
    private Date fechaConsulta;
    @Lob
    @Column(name = "imagen")
    private byte[] imagen;
    @Size(max = 50)
    @Column(name = "tipo_imagen")
    private String tipoImagen;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "detalleHistorial")
    private Collection<VacunaDetalle> vacunaDetalleCollection;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne
    private Usuarios idUsuario;

    public DetalleHistorial() {
    }

    public DetalleHistorial(Integer idDetalle) {
        this.idDetalle = idDetalle;
    }

    public Integer getIdDetalle() {
        return idDetalle;
    }

    public void setIdDetalle(Integer idDetalle) {
        this.idDetalle = idDetalle;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Date getFechaConsulta() {
        return fechaConsulta;
    }

    public void setFechaConsulta(Date fechaConsulta) {
        this.fechaConsulta = fechaConsulta;
    }

    public byte[] getImagen() {
        return imagen;
    }

    public void setImagen(byte[] imagen) {
        this.imagen = imagen;
    }

    public String getTipoImagen() {
        return tipoImagen;
    }

    public void setTipoImagen(String tipoImagen) {
        this.tipoImagen = tipoImagen;
    }

    @XmlTransient
    public Collection<VacunaDetalle> getVacunaDetalleCollection() {
        return vacunaDetalleCollection;
    }

    public void setVacunaDetalleCollection(Collection<VacunaDetalle> vacunaDetalleCollection) {
        this.vacunaDetalleCollection = vacunaDetalleCollection;
    }

    public Usuarios getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuarios idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDetalle != null ? idDetalle.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalleHistorial)) {
            return false;
        }
        DetalleHistorial other = (DetalleHistorial) object;
        if ((this.idDetalle == null && other.idDetalle != null) || (this.idDetalle != null && !this.idDetalle.equals(other.idDetalle))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.DetalleHistorial[ idDetalle=" + idDetalle + " ]";
    }
    
}
