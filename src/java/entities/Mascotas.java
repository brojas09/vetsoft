/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author broja
 */
@Entity
@Table(name = "mascotas", catalog = "vetsoft", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Mascotas.findAll", query = "SELECT m FROM Mascotas m"),
    @NamedQuery(name = "Mascotas.findByIdMascota", query = "SELECT m FROM Mascotas m WHERE m.idMascota = :idMascota"),
    @NamedQuery(name = "Mascotas.findByFechaAlta", query = "SELECT m FROM Mascotas m WHERE m.fechaAlta = :fechaAlta"),
    @NamedQuery(name = "Mascotas.findByFechaNacimiento", query = "SELECT m FROM Mascotas m WHERE m.fechaNacimiento = :fechaNacimiento"),
    @NamedQuery(name = "Mascotas.findByNombreMascota", query = "SELECT m FROM Mascotas m WHERE m.nombreMascota = :nombreMascota"),
    @NamedQuery(name = "Mascotas.findByActivo", query = "SELECT m FROM Mascotas m WHERE m.activo = :activo"),
    @NamedQuery(name = "Mascotas.findByMime", query = "SELECT m FROM Mascotas m WHERE m.mime = :mime"),
    @NamedQuery(name = "Mascotas.findByPelaje", query = "SELECT m FROM Mascotas m WHERE m.pelaje = :pelaje")})
public class Mascotas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_mascota")
    private Integer idMascota;
    @Column(name = "fecha_alta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAlta;
    @Column(name = "fecha_nacimiento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaNacimiento;
    @Size(max = 50)
    @Column(name = "nombre_mascota")
    private String nombreMascota;
    @Column(name = "activo")
    private Boolean activo;
    @Size(max = 50)
    @Column(name = "mime")
    private String mime;
    @Lob
    @Column(name = "archivo")
    private byte[] archivo;
    @Size(max = 50)
    @Column(name = "pelaje")
    private String pelaje;
    @JoinColumn(name = "id_propietario", referencedColumnName = "id_propietarios")
    @ManyToOne(optional = false)
    private Propietarios idPropietario;
    @JoinColumn(name = "id_raza", referencedColumnName = "id_razas")
    @ManyToOne(optional = false)
    private Razas idRaza;
    @JoinColumn(name = "id_veterinaria", referencedColumnName = "id_veterinaria")
    @ManyToOne(optional = false)
    private Veterinarias idVeterinaria;

    public Mascotas() {
    }

    public Mascotas(Integer idMascota) {
        this.idMascota = idMascota;
    }

    public Integer getIdMascota() {
        return idMascota;
    }

    public void setIdMascota(Integer idMascota) {
        this.idMascota = idMascota;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getNombreMascota() {
        return nombreMascota;
    }

    public void setNombreMascota(String nombreMascota) {
        this.nombreMascota = nombreMascota;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getMime() {
        return mime;
    }

    public void setMime(String mime) {
        this.mime = mime;
    }

    public byte[] getArchivo() {
        return archivo;
    }

    public void setArchivo(byte[] archivo) {
        this.archivo = archivo;
    }

    public String getPelaje() {
        return pelaje;
    }

    public void setPelaje(String pelaje) {
        this.pelaje = pelaje;
    }

    public Propietarios getIdPropietario() {
        return idPropietario;
    }

    public void setIdPropietario(Propietarios idPropietario) {
        this.idPropietario = idPropietario;
    }

    public Razas getIdRaza() {
        return idRaza;
    }

    public void setIdRaza(Razas idRaza) {
        this.idRaza = idRaza;
    }

    public Veterinarias getIdVeterinaria() {
        return idVeterinaria;
    }

    public void setIdVeterinaria(Veterinarias idVeterinaria) {
        this.idVeterinaria = idVeterinaria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMascota != null ? idMascota.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mascotas)) {
            return false;
        }
        Mascotas other = (Mascotas) object;
        if ((this.idMascota == null && other.idMascota != null) || (this.idMascota != null && !this.idMascota.equals(other.idMascota))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Mascotas[ idMascota=" + idMascota + " ]";
    }
    
}
