/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author broja
 */
@Entity
@Table(name = "propietarios", catalog = "vetsoft", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Propietarios.findAll", query = "SELECT p FROM Propietarios p"),
    @NamedQuery(name = "Propietarios.findByIdPropietarios", query = "SELECT p FROM Propietarios p WHERE p.idPropietarios = :idPropietarios"),
    @NamedQuery(name = "Propietarios.findByNombrePropietario", query = "SELECT p FROM Propietarios p WHERE p.nombrePropietario = :nombrePropietario"),
    @NamedQuery(name = "Propietarios.findByDireccion", query = "SELECT p FROM Propietarios p WHERE p.direccion = :direccion"),
    @NamedQuery(name = "Propietarios.findByTelefono", query = "SELECT p FROM Propietarios p WHERE p.telefono = :telefono"),
    @NamedQuery(name = "Propietarios.findByRuc", query = "SELECT p FROM Propietarios p WHERE p.ruc = :ruc"),
    @NamedQuery(name = "Propietarios.findByActivo", query = "SELECT p FROM Propietarios p WHERE p.activo = :activo"),
    @NamedQuery(name = "Propietarios.findByEmail", query = "SELECT p FROM Propietarios p WHERE p.email = :email")})
public class Propietarios implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_propietarios")
    private Integer idPropietarios;
    @Size(max = 50)
    @Column(name = "nombre_propietario")
    private String nombrePropietario;
    @Size(max = 50)
    @Column(name = "direccion")
    private String direccion;
    @Size(max = 50)
    @Column(name = "telefono")
    private String telefono;
    @Size(max = 50)
    @Column(name = "ruc")
    private String ruc;
    @Column(name = "activo")
    private Boolean activo;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 50)
    @Column(name = "email")
    private String email;
    @JoinColumn(name = "id_veterinaria", referencedColumnName = "id_veterinaria")
    @ManyToOne
    private Veterinarias idVeterinaria;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPropietario")
    private Collection<Mascotas> mascotasCollection;

    public Propietarios() {
    }

    public Propietarios(Integer idPropietarios) {
        this.idPropietarios = idPropietarios;
    }

    public Integer getIdPropietarios() {
        return idPropietarios;
    }

    public void setIdPropietarios(Integer idPropietarios) {
        this.idPropietarios = idPropietarios;
    }

    public String getNombrePropietario() {
        return nombrePropietario;
    }

    public void setNombrePropietario(String nombrePropietario) {
        this.nombrePropietario = nombrePropietario;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Veterinarias getIdVeterinaria() {
        return idVeterinaria;
    }

    public void setIdVeterinaria(Veterinarias idVeterinaria) {
        this.idVeterinaria = idVeterinaria;
    }

    @XmlTransient
    public Collection<Mascotas> getMascotasCollection() {
        return mascotasCollection;
    }

    public void setMascotasCollection(Collection<Mascotas> mascotasCollection) {
        this.mascotasCollection = mascotasCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPropietarios != null ? idPropietarios.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Propietarios)) {
            return false;
        }
        Propietarios other = (Propietarios) object;
        if ((this.idPropietarios == null && other.idPropietarios != null) || (this.idPropietarios != null && !this.idPropietarios.equals(other.idPropietarios))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Propietarios[ idPropietarios=" + idPropietarios + " ]";
    }
    
}
