/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author broja
 */
@Entity
@Table(name = "razas", catalog = "vetsoft", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Razas.findAll", query = "SELECT r FROM Razas r"),
    @NamedQuery(name = "Razas.findByIdRazas", query = "SELECT r FROM Razas r WHERE r.idRazas = :idRazas"),
    @NamedQuery(name = "Razas.findByNombreRaza", query = "SELECT r FROM Razas r WHERE r.nombreRaza = :nombreRaza")})
public class Razas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_razas")
    private Integer idRazas;
    @Size(max = 50)
    @Column(name = "nombre_raza")
    private String nombreRaza;
    @JoinColumn(name = "id_especie", referencedColumnName = "id_especie")
    @ManyToOne
    private Especies idEspecie;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idRaza")
    private Collection<Mascotas> mascotasCollection;

    public Razas() {
    }

    public Razas(Integer idRazas) {
        this.idRazas = idRazas;
    }

    public Integer getIdRazas() {
        return idRazas;
    }

    public void setIdRazas(Integer idRazas) {
        this.idRazas = idRazas;
    }

    public String getNombreRaza() {
        return nombreRaza;
    }

    public void setNombreRaza(String nombreRaza) {
        this.nombreRaza = nombreRaza;
    }

    public Especies getIdEspecie() {
        return idEspecie;
    }

    public void setIdEspecie(Especies idEspecie) {
        this.idEspecie = idEspecie;
    }

    @XmlTransient
    public Collection<Mascotas> getMascotasCollection() {
        return mascotasCollection;
    }

    public void setMascotasCollection(Collection<Mascotas> mascotasCollection) {
        this.mascotasCollection = mascotasCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRazas != null ? idRazas.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Razas)) {
            return false;
        }
        Razas other = (Razas) object;
        if ((this.idRazas == null && other.idRazas != null) || (this.idRazas != null && !this.idRazas.equals(other.idRazas))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Razas[ idRazas=" + idRazas + " ]";
    }
    
}
