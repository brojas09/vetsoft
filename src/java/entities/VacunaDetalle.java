/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author broja
 */
@Entity
@Table(name = "vacuna_detalle", catalog = "vetsoft", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VacunaDetalle.findAll", query = "SELECT v FROM VacunaDetalle v"),
    @NamedQuery(name = "VacunaDetalle.findByIdDetalle", query = "SELECT v FROM VacunaDetalle v WHERE v.vacunaDetallePK.idDetalle = :idDetalle"),
    @NamedQuery(name = "VacunaDetalle.findByIdVacuna", query = "SELECT v FROM VacunaDetalle v WHERE v.vacunaDetallePK.idVacuna = :idVacuna"),
    @NamedQuery(name = "VacunaDetalle.findByProximaDosis", query = "SELECT v FROM VacunaDetalle v WHERE v.proximaDosis = :proximaDosis")})
public class VacunaDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected VacunaDetallePK vacunaDetallePK;
    @Column(name = "proxima_dosis")
    @Temporal(TemporalType.TIMESTAMP)
    private Date proximaDosis;
    @JoinColumn(name = "id_detalle", referencedColumnName = "id_detalle", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private DetalleHistorial detalleHistorial;
    @JoinColumn(name = "id_vacuna", referencedColumnName = "id_vacuna", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Vacunas vacunas;

    public VacunaDetalle() {
    }

    public VacunaDetalle(VacunaDetallePK vacunaDetallePK) {
        this.vacunaDetallePK = vacunaDetallePK;
    }

    public VacunaDetalle(int idDetalle, int idVacuna) {
        this.vacunaDetallePK = new VacunaDetallePK(idDetalle, idVacuna);
    }

    public VacunaDetallePK getVacunaDetallePK() {
        return vacunaDetallePK;
    }

    public void setVacunaDetallePK(VacunaDetallePK vacunaDetallePK) {
        this.vacunaDetallePK = vacunaDetallePK;
    }

    public Date getProximaDosis() {
        return proximaDosis;
    }

    public void setProximaDosis(Date proximaDosis) {
        this.proximaDosis = proximaDosis;
    }

    public DetalleHistorial getDetalleHistorial() {
        return detalleHistorial;
    }

    public void setDetalleHistorial(DetalleHistorial detalleHistorial) {
        this.detalleHistorial = detalleHistorial;
    }

    public Vacunas getVacunas() {
        return vacunas;
    }

    public void setVacunas(Vacunas vacunas) {
        this.vacunas = vacunas;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vacunaDetallePK != null ? vacunaDetallePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VacunaDetalle)) {
            return false;
        }
        VacunaDetalle other = (VacunaDetalle) object;
        if ((this.vacunaDetallePK == null && other.vacunaDetallePK != null) || (this.vacunaDetallePK != null && !this.vacunaDetallePK.equals(other.vacunaDetallePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.VacunaDetalle[ vacunaDetallePK=" + vacunaDetallePK + " ]";
    }
    
}
