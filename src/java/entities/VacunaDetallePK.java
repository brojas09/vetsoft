/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author broja
 */
@Embeddable
public class VacunaDetallePK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_detalle")
    private int idDetalle;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_vacuna")
    private int idVacuna;

    public VacunaDetallePK() {
    }

    public VacunaDetallePK(int idDetalle, int idVacuna) {
        this.idDetalle = idDetalle;
        this.idVacuna = idVacuna;
    }

    public int getIdDetalle() {
        return idDetalle;
    }

    public void setIdDetalle(int idDetalle) {
        this.idDetalle = idDetalle;
    }

    public int getIdVacuna() {
        return idVacuna;
    }

    public void setIdVacuna(int idVacuna) {
        this.idVacuna = idVacuna;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idDetalle;
        hash += (int) idVacuna;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VacunaDetallePK)) {
            return false;
        }
        VacunaDetallePK other = (VacunaDetallePK) object;
        if (this.idDetalle != other.idDetalle) {
            return false;
        }
        if (this.idVacuna != other.idVacuna) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.VacunaDetallePK[ idDetalle=" + idDetalle + ", idVacuna=" + idVacuna + " ]";
    }
    
}
