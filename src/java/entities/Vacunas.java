/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author broja
 */
@Entity
@Table(name = "vacunas", catalog = "vetsoft", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vacunas.findAll", query = "SELECT v FROM Vacunas v"),
    @NamedQuery(name = "Vacunas.findByIdVacuna", query = "SELECT v FROM Vacunas v WHERE v.idVacuna = :idVacuna"),
    @NamedQuery(name = "Vacunas.findByNombreVacuna", query = "SELECT v FROM Vacunas v WHERE v.nombreVacuna = :nombreVacuna")})
public class Vacunas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_vacuna")
    private Integer idVacuna;
    @Size(max = 50)
    @Column(name = "nombre_vacuna")
    private String nombreVacuna;
    @JoinTable(name = "vacuna_veterinaria", joinColumns = {
        @JoinColumn(name = "id_vacuna", referencedColumnName = "id_vacuna")}, inverseJoinColumns = {
        @JoinColumn(name = "id_veterinaria", referencedColumnName = "id_veterinaria")})
    @ManyToMany
    private Collection<Veterinarias> veterinariasCollection;
    @ManyToMany(mappedBy = "vacunasCollection")
    private Collection<Especies> especiesCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vacunas")
    private Collection<VacunaDetalle> vacunaDetalleCollection;

    public Vacunas() {
    }

    public Vacunas(Integer idVacuna) {
        this.idVacuna = idVacuna;
    }

    public Integer getIdVacuna() {
        return idVacuna;
    }

    public void setIdVacuna(Integer idVacuna) {
        this.idVacuna = idVacuna;
    }

    public String getNombreVacuna() {
        return nombreVacuna;
    }

    public void setNombreVacuna(String nombreVacuna) {
        this.nombreVacuna = nombreVacuna;
    }

    @XmlTransient
    public Collection<Veterinarias> getVeterinariasCollection() {
        return veterinariasCollection;
    }

    public void setVeterinariasCollection(Collection<Veterinarias> veterinariasCollection) {
        this.veterinariasCollection = veterinariasCollection;
    }

    @XmlTransient
    public Collection<Especies> getEspeciesCollection() {
        return especiesCollection;
    }

    public void setEspeciesCollection(Collection<Especies> especiesCollection) {
        this.especiesCollection = especiesCollection;
    }

    @XmlTransient
    public Collection<VacunaDetalle> getVacunaDetalleCollection() {
        return vacunaDetalleCollection;
    }

    public void setVacunaDetalleCollection(Collection<VacunaDetalle> vacunaDetalleCollection) {
        this.vacunaDetalleCollection = vacunaDetalleCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVacuna != null ? idVacuna.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vacunas)) {
            return false;
        }
        Vacunas other = (Vacunas) object;
        if ((this.idVacuna == null && other.idVacuna != null) || (this.idVacuna != null && !this.idVacuna.equals(other.idVacuna))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Vacunas[ idVacuna=" + idVacuna + " ]";
    }
    
}
