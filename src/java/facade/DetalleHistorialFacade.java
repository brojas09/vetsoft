/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package facade;

import entities.DetalleHistorial;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author broja
 */
@Stateless
public class DetalleHistorialFacade extends AbstractFacade<DetalleHistorial> {
    @PersistenceContext(unitName = "vetsoftPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DetalleHistorialFacade() {
        super(DetalleHistorial.class);
    }
    
}
