/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import entities.Mascotas;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author broja
 */
@Stateless
public class MascotasFacade extends AbstractFacade<Mascotas> {

    @PersistenceContext(unitName = "vetsoftPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MascotasFacade() {
        super(Mascotas.class);
    }

    public List<Mascotas> findAll(Integer idVeterinaria) {
        Query q = getEntityManager().createNamedQuery("Mascotas.getMascotasByVeterinarias");
        q.setParameter("idVeterinaria", idVeterinaria);
        if (q.getResultList().isEmpty() != true) {
            return q.getResultList();
        } else {
            return null;
        }
    }
}
