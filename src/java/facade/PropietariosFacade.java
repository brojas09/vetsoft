/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import entities.Propietarios;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author broja
 */
@Stateless
public class PropietariosFacade extends AbstractFacade<Propietarios> {

    @PersistenceContext(unitName = "vetsoftPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PropietariosFacade() {
        super(Propietarios.class);
    }

    public List<Propietarios> findAll(Integer idVeterinaria) {
        Query q = getEntityManager().createNamedQuery("Propietarios.getPropietariosByVeterinarias");
        q.setParameter("idVeterinaria", idVeterinaria);
        if (q.getResultList().isEmpty() != true) {
            return q.getResultList();
        } else {
            return null;
        }
    }

    public Propietarios find(Integer id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("vetsoftPU");
        em = emf.createEntityManager();
        return em.find(Propietarios.class, id);
    }
}
