/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import entities.Razas;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

/**
 *
 * @author broja
 */
@Stateless
public class RazasFacade extends AbstractFacade<Razas> {

    @PersistenceContext(unitName = "vetsoftPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RazasFacade() {
        super(Razas.class);
    }

    public Razas find(Integer id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("vetsoftPU");
        em = emf.createEntityManager();
        return em.find(Razas.class, id);
    }
}
