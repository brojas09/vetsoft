/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import entities.Usuarios;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author broja
 */
@Stateless
public class UsuariosFacade extends AbstractFacade<Usuarios> {

    @PersistenceContext(unitName = "vetsoftPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuariosFacade() {
        super(Usuarios.class);
    }

    public Usuarios findByUsuario(String user) {
        Query q = getEntityManager().createNamedQuery("Usuarios.findByUsuario");
        q.setParameter("usuario", user);
        if (q.getResultList().isEmpty() != true) {
            return (Usuarios) q.getResultList().get(0);
        } else {
            return null;
        }
    }

    public Usuarios find(Integer id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("vetsoftPU");
        em = emf.createEntityManager();
        return em.find(Usuarios.class, id);
    }

    public int updatePass(Usuarios u) {
        em.joinTransaction();
        try {
            String query = "Update Usuarios set password = '" + u.getPassword() + "' where id_usuario = " + u.getIdUsuario();
            System.out.println(query);
            Query q = getEntityManager().createNativeQuery(query);
            int result = q.executeUpdate();
            em.clear();
            return result;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return -1;
        }
    }

    public int updateUser(Usuarios u) {
        try {
            super.edit(u);
            return 1;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return -1;
        }    
    }
}
