/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import entities.Veterinarias;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

/**
 *
 * @author broja
 */
@Stateless
public class VeterinariasFacade extends AbstractFacade<Veterinarias> {

    @PersistenceContext(unitName = "vetsoftPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VeterinariasFacade() {
        super(Veterinarias.class);
    }

    public Veterinarias find(Integer id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("vetsoftPU");
        em = emf.createEntityManager();
        return em.find(Veterinarias.class, id);
    }
}
