/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.springframework.samples.petclinic.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
//import javax.validation.constraints.NotNull;

/**
 *
 * @author lg_more
 */
@Entity
@Table(name = "persona")
//@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "Persona.findAll", query = "SELECT p FROM Persona p")
//    , @NamedQuery(name = "Persona.findByIdPersona", query = "SELECT p FROM Persona p WHERE p.idPersona = :idPersona")
//    , @NamedQuery(name = "Persona.findByRazonSocial", query = "SELECT p FROM Persona p WHERE p.razonSocial = :razonSocial")
//    , @NamedQuery(name = "Persona.findByActivo", query = "SELECT p FROM Persona p WHERE p.activo = :activo")
//    , @NamedQuery(name = "Persona.findByEmail", query = "SELECT p FROM Persona p WHERE p.email = :email")
//    , @NamedQuery(name = "Persona.findByTelefono", query = "SELECT p FROM Persona p WHERE p.telefono = :telefono")
//    , @NamedQuery(name = "Persona.findByRuc", query = "SELECT p FROM Persona p WHERE p.ruc = :ruc")})
public class Persona implements Serializable {

////    private static final long serialVersionUID = 1L;
    @Id
//    @Basic(optional = false)
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotEmpty
//    @Size(min = 1, max = 2147483647)
    @Column(name = "razon_social")
    private String razonSocial;
//    @Basic(optional = false)
    @NotNull
    @Column(name = "activo")
    private boolean activo;
    @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
//    @Basic(optional = false)
    @NotEmpty
//    @Size(min = 1, max = 2147483647)
    @Column(name = "email")
    private String email;
//    @Basic(optional = false)
    @NotEmpty
    @Size(min = 10, max = 10)
    @Column(name = "telefono")
    private String telefono;
//    @Basic(optional = false)
    @NotEmpty
//    @Size(min = 1, max = 2147483647)
    @Column(name = "ruc")
    private String ruc;
    @JoinColumn(name = "tipo", referencedColumnName = "id_tipo_persona")
    @ManyToOne(optional = false)
    private TipoPersona tipoPersona;

    public Persona() {
        this.id = 0;
        this.razonSocial = "";
        this.activo = true;
        this.email = "";
        this.telefono = "";
        this.ruc = "";
        this.tipoPersona=new TipoPersona();
    }

//    public Persona(Integer idPersona) {
//        this.idPersona = idPersona;
//    }
    public Persona(Integer id, String razonSocial, boolean activo, String email, String telefono, String ruc, TipoPersona tipo) {
        this.id = (id);
        this.razonSocial = razonSocial;
        this.activo = activo;
        this.email = email;
        this.telefono = telefono;
        this.ruc = ruc;
        this.tipoPersona=tipo;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public boolean getActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

//    public TipoPersona getTipo() {
//        return tipo;
//    }
//
//    public void setTipo(TipoPersona tipo) {
//        this.tipo = tipo;
//    }
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (this.getId() != null ? this.getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Persona)) {
            return false;
        }
        Persona other = (Persona) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.id.equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.springframework.samples.petclinic.model.Persona[ idPersona=" + this.getId() + " ]";
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the tipo
     */
    public TipoPersona getTipoPersona() {
        return tipoPersona;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipoPersona(TipoPersona tipo) {
        this.tipoPersona = tipo;
    }

}
