/*
 * Copyright 2012-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.persona;

//import org.springframework.samples.petclinic.propietario.*;
//import org.springframework.samples.petclinic.owner.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.samples.petclinic.model.Persona;
import org.springframework.samples.petclinic.model.TipoPersona;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author Juergen Hoeller
 * @author Ken Krebs
 * @author Arjen Poutsma
 * @author Michael Isvy
 */
@Controller
class PersonaController {

    private static final String VIEWS_OWNER_CREATE_OR_UPDATE_FORM = "personas/createOrUpdatePersonaForm";
    private final PersonaRepository personas;
    private final TipoPersonaRepository tipoPersonas;


    public PersonaController(PersonaRepository personaService, TipoPersonaRepository tipoPersonaService) {
        this.personas = personaService;
        this.tipoPersonas=tipoPersonaService;
    }

//    @InitBinder
//    public void setAllowedFields(WebDataBinder dataBinder) {
//        dataBinder.setDisallowedFields("id");
//    }

    @GetMapping("/personas/new")
    public String initCreationForm(Map<String, Object> model) {
        Persona owner = new Persona();
        model.put("persona", owner);
        model.put("tipoPersonas", tipoPersonas.findAll());
        return VIEWS_OWNER_CREATE_OR_UPDATE_FORM;
    }

    @PostMapping("/personas/new")
    public String processCreationForm(@Valid Persona owner, BindingResult result, Map<String, Object> model) {
        if (result.hasErrors()) {
            model.put("tipoPersonas", tipoPersonas.findAll());
            return VIEWS_OWNER_CREATE_OR_UPDATE_FORM;
        } else {
            this.personas.save(owner);
            return "redirect:/personas/" + owner.getId();
        }
    }

    @GetMapping("/personas/find")
    public String initFindForm(Map<String, Object> model) {
        model.put("persona", new Persona());
        return "personas/findPersonas";
    }

    @GetMapping("/personas")
    public String processFindForm(Persona owner, BindingResult result, Map<String, Object> model) {

        // allow parameterless GET request for /owners to return all records
        if (owner.getRazonSocial() == null) {
            owner.setRazonSocial(""); // empty string signifies broadest possible search
        }

        // find owners by last name
        Collection<Persona> results = this.personas.findByRazonSocial(owner.getRazonSocial());
        if (results.isEmpty()) {
            // no owners found
            result.rejectValue("lastName", "notFound", "not found");
            return "personas/findPersonas";
        } else if (results.size() == 1) {
            // 1 owner found
            owner = results.iterator().next();
            return "redirect:/personas/" + owner.getId();
        } else {
            // multiple owners found
            model.put("selections", results);
            return "personas/personasList";
        }
    }

    @GetMapping("/personas/{personaId}/edit")
    public String initUpdateOwnerForm(@PathVariable("personaId") int personaId, Model model) {
        Persona owner = this.personas.findById(personaId);
        TipoPersona tipoPersona = this.tipoPersonas.findById(owner.getTipoPersona().getIdTipoPersona());
        model.addAttribute(owner);
        model.addAttribute("tipoPersonas", tipoPersonas.findAll());
        return VIEWS_OWNER_CREATE_OR_UPDATE_FORM;
    }

    @PostMapping("/personas/{ownerId}/edit")
    public String processUpdateOwnerForm(@Valid Persona owner, BindingResult result, @PathVariable("ownerId") int ownerId) {
        if (result.hasErrors()) {
            return VIEWS_OWNER_CREATE_OR_UPDATE_FORM;
        } else {
            owner.setId(ownerId);
            this.personas.save(owner);
            return "redirect:/personas/{ownerId}";
        }
    }

    // RESTful method
    @RequestMapping(value = "/personas/{personaId}", 
            method=RequestMethod.GET, 
            consumes = {"application/json","application/xml"},
            produces = { "application/json", "application/xml" })
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    Persona showPersonaWithMarshalling(@PathVariable("personaId") int personaId) {
        return this.personas.findById(personaId);
    }



    /**
     * Custom handler for displaying an owner.
     *
     * @param ownerId the ID of the owner to display
     * @return a ModelMap with the model attributes for the view
//     */
    @RequestMapping(value="/personas/{personaId}", 
            method=RequestMethod.GET)
    public ModelAndView showPersona(@RequestHeader(value = "Accept", required = false)
                                        String acceptHeader,@PathVariable("personaId") int personaId) {
        ModelAndView mav = new ModelAndView("personas/personaDetails");
        mav.addObject(this.personas.findById(personaId));
        return mav;
    }


}
