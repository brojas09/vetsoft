/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.springframework.samples.petclinic.veterinarias;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
//import org.springframework.samples.petclinic.propietario.Propietarios;

/**
 *
 * @author lg_more
 */
@Entity
@Table(name = "veterinarias")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Veterinarias.findAll", query = "SELECT v FROM Veterinarias v")
    , @NamedQuery(name = "Veterinarias.findByIdVeterinaria", query = "SELECT v FROM Veterinarias v WHERE v.idVeterinaria = :idVeterinaria")
    , @NamedQuery(name = "Veterinarias.findByRazonSocial", query = "SELECT v FROM Veterinarias v WHERE v.razonSocial = :razonSocial")
    , @NamedQuery(name = "Veterinarias.findByDireccion", query = "SELECT v FROM Veterinarias v WHERE v.direccion = :direccion")
    , @NamedQuery(name = "Veterinarias.findByTelefono", query = "SELECT v FROM Veterinarias v WHERE v.telefono = :telefono")
    , @NamedQuery(name = "Veterinarias.findByRuc", query = "SELECT v FROM Veterinarias v WHERE v.ruc = :ruc")
    , @NamedQuery(name = "Veterinarias.findByFechaAlta", query = "SELECT v FROM Veterinarias v WHERE v.fechaAlta = :fechaAlta")
    , @NamedQuery(name = "Veterinarias.findByEstado", query = "SELECT v FROM Veterinarias v WHERE v.estado = :estado")})
public class Veterinarias implements Serializable {

    @Size(max = 50)
    @Column(name = "razon_social")
    private String razonSocial;
    @Size(max = 50)
    @Column(name = "direccion")
    private String direccion;
    @Size(max = 50)
    @Column(name = "telefono")
    private String telefono;
    @Size(max = 50)
    @Column(name = "ruc")
    private String ruc;
//    @OneToMany(mappedBy = "idVeterinaria")
//    private Collection<Propietarios> propietariosCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_veterinaria")
    private Integer idVeterinaria;
    @Column(name = "fecha_alta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAlta;
    @Column(name = "estado")
    private Boolean estado;
    @JoinColumn(name = "propietario", referencedColumnName = "id_usuario")
    @ManyToOne
    private Usuarios propietario;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idVeterinaria")
    private Set<Usuarios> usuariosSet;

    public Veterinarias() {
    }

    public Veterinarias(Integer idVeterinaria) {
        this.idVeterinaria = idVeterinaria;
    }

    public Integer getIdVeterinaria() {
        return idVeterinaria;
    }

    public void setIdVeterinaria(Integer idVeterinaria) {
        this.idVeterinaria = idVeterinaria;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }


    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Usuarios getPropietario() {
        return propietario;
    }

    public void setPropietario(Usuarios propietario) {
        this.propietario = propietario;
    }

    @XmlTransient
    public Set<Usuarios> getUsuariosSet() {
        return usuariosSet;
    }

    public void setUsuariosSet(Set<Usuarios> usuariosSet) {
        this.usuariosSet = usuariosSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVeterinaria != null ? idVeterinaria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Veterinarias)) {
            return false;
        }
        Veterinarias other = (Veterinarias) object;
        if ((this.idVeterinaria == null && other.idVeterinaria != null) || (this.idVeterinaria != null && !this.idVeterinaria.equals(other.idVeterinaria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.springframework.samples.petclinic.veterinarias.Veterinarias[ idVeterinaria=" + idVeterinaria + " ]";
    }


    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

//    @XmlTransient
//    public Collection<Propietarios> getPropietariosCollection() {
//        return propietariosCollection;
//    }
//
//    public void setPropietariosCollection(Collection<Propietarios> propietariosCollection) {
//        this.propietariosCollection = propietariosCollection;
//    }
    
}
